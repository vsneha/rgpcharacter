﻿using RPGCharacters.Aspects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.weapon

{
    public class Weapon : Item
    {
        public Weapons WeaponType { get; set; }

        public WeaponAttributes WeaponAttributes { get; set;}
    }
}