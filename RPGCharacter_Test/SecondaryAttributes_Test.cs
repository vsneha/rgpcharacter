﻿using RPGCharacters.Character;
using System;
using Xunit;

namespace RPGCharacters_Test
{
    public class SecondaryAttributes_Tests
    {
        [Fact]
        public void Test_Create_Mage()
        {
            Mage mageCharacter = new Mage("MAGE");
            Assert.Equal(5, mageCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(1, mageCharacter.PrimaryAttributes.Strength);
            Assert.Equal(1, mageCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(8, mageCharacter.PrimaryAttributes.Intelligence);
            Assert.Equal(50, mageCharacter.SecondaryAttributes.Health);
            Assert.Equal(2, mageCharacter.SecondaryAttributes.ArmorRating);
            Assert.Equal(8, mageCharacter.SecondaryAttributes.ElementalResistance);
            Assert.Equal(0, mageCharacter.Damage);
        }

        [Fact]
        public void Test_Create_Ranger()
        {
            Ranger rangerCharacter = new Ranger("Ranger");
            Assert.Equal(8, rangerCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(1, rangerCharacter.PrimaryAttributes.Strength);
            Assert.Equal(7, rangerCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(1, rangerCharacter.PrimaryAttributes.Intelligence);
            Assert.Equal(80, rangerCharacter.SecondaryAttributes.Health);
            Assert.Equal(8, rangerCharacter.SecondaryAttributes.ArmorRating);
            Assert.Equal(1, rangerCharacter.SecondaryAttributes.ElementalResistance);
            Assert.Equal(0, rangerCharacter.Damage);

        }

        [Fact]
        public void Test_Create_Rogue()
        {
            Rogue rogueCharacter = new Rogue("Rogue");
            Assert.Equal(8, rogueCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(2, rogueCharacter.PrimaryAttributes.Strength);
            Assert.Equal(6, rogueCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(1, rogueCharacter.PrimaryAttributes.Intelligence);
            Assert.Equal(80, rogueCharacter.SecondaryAttributes.Health);
            Assert.Equal(8, rogueCharacter.SecondaryAttributes.ArmorRating);
            Assert.Equal(1, rogueCharacter.SecondaryAttributes.ElementalResistance);
            Assert.Equal(0, rogueCharacter.Damage);

        }

        [Fact]
        public void Test_Create_Warrior()
        {
            Warrior warriorCharacter = new Warrior("Warrior");
            Assert.Equal(10, warriorCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(5, warriorCharacter.PrimaryAttributes.Strength);
            Assert.Equal(2, warriorCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(1, warriorCharacter.PrimaryAttributes.Intelligence);
            Assert.Equal(100, warriorCharacter.SecondaryAttributes.Health);
            Assert.Equal(7, warriorCharacter.SecondaryAttributes.ArmorRating);
            Assert.Equal(1, warriorCharacter.SecondaryAttributes.ElementalResistance);
            Assert.Equal(0, warriorCharacter.Damage);


        }
    }
}
