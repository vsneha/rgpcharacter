﻿using System;
using Xunit;
using RPGCharacters.Character;

namespace RPGCharacters_Test

{
    public class PrimaryAttributes_Test
    {
        [Fact]
        public void Testing_Mage_PrimaryAtrributes_Values()
        {
            Mage mageCharacter = new Mage("Mage");
            Assert.Equal(5, mageCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(1, mageCharacter.PrimaryAttributes.Strength);
            Assert.Equal(1, mageCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(8, mageCharacter.PrimaryAttributes.Intelligence);

        }

        [Fact]
        public void Testing_Ranger_PrimaryAtrributes_Values()
        {

            Ranger rangerCharacter = new Ranger("Ranger");
            Assert.Equal(8, rangerCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(1, rangerCharacter.PrimaryAttributes.Strength);
            Assert.Equal(7, rangerCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(1, rangerCharacter.PrimaryAttributes.Intelligence);


        }

        [Fact]
        public void Testing_Rogue_PrimaryAtrributes_Values()
        {
            Rogue rogueCharacter = new Rogue("Rogue");
            Assert.Equal(8, rogueCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(2, rogueCharacter.PrimaryAttributes.Strength);
            Assert.Equal(6, rogueCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(1, rogueCharacter.PrimaryAttributes.Intelligence);

        }

        [Fact]
        public void Testing_Warrior_PrimaryAtrributes_Values()
        {

            Warrior warriorCharacter = new Warrior("Warrior");
            Assert.Equal(10, warriorCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(5, warriorCharacter.PrimaryAttributes.Strength);
            Assert.Equal(2, warriorCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(1, warriorCharacter.PrimaryAttributes.Intelligence);


        }


    }
}

